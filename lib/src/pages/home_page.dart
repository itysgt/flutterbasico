import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:itys_app/src/pages/login_page.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home"),
          actions: [
            IconButton(
              icon: Icon(Icons.power_settings_new),
              onPressed: () {
                FirebaseAuth.instance.signOut().then((result) =>
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => LoginPage())));
              },
            )
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showDialogMensaje(false, "");
          },
          child: Icon(Icons.add),
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestore.instance
              .collection("mensajes")
              .orderBy("fechaHora", descending: true)
              .snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text('Algo salió mal'));
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: Text("Cargando"));
            }

            return new ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                return Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10)),
                  margin: EdgeInsets.all(10),
                  elevation: 10,
                  child: ListTile(
                    leading: Icon(Icons.ac_unit),
                    title: Text('Usuario: ${document['usuario']}'),
                    subtitle: Text("Mensaje: ${document['mensaje']}"),
                    trailing: Wrap(
                      children: [
                        IconButton(
                            onPressed: () {
                              showDialogMensaje(true, document['docID']);
                            },
                            icon: Icon(Icons.edit, color: Colors.orangeAccent)),
                        IconButton(
                            onPressed: () {
                              FirebaseFirestore.instance
                                  .collection("mensajes")
                                  .doc(document["docID"])
                                  .delete();
                            },
                            icon: Icon(Icons.delete, color: Colors.red)),
                      ],
                    ),
                  ),
                );
              }).toList(),
            );
          },
        ));
  }

  void showDialogMensaje(bool actualizar, String docID) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        var mensajeController = TextEditingController();
        return AlertDialog(
          scrollable: true,
          title: Text('Mensaje'),
          content: Column(
            children: [
              TextFormField(
                controller: mensajeController,
                decoration: InputDecoration(
                    hintText: 'Mensaje', prefixIcon: Icon(Icons.sms)),
              ),
            ],
          ),
          actions: [
            TextButton(
              onPressed: () => Navigator.pop(context),
              child: Text('Cancelar'),
            ),
            TextButton(
              onPressed: () {
                var mensaje = mensajeController.text;
                User? user = FirebaseAuth.instance.currentUser;

                if (actualizar) {
                  FirebaseFirestore.instance
                      .collection("mensajes")
                      .doc(docID)
                      .update({
                    "mensaje": mensaje.toString(),
                  }).then((value) => Navigator.pop(context));
                } else {
                  DocumentReference docRef =
                      FirebaseFirestore.instance.collection("mensajes").doc();

                  docRef.set({
                    "docID": docRef.id,
                    "usuario": "${user!.email}",
                    "mensaje": mensaje.toString(),
                    "fechaHora": DateTime.now()
                  }).then((value) => Navigator.pop(context));
                }
              },
              child: Text('Guardar'),
            ),
          ],
        );
      },
    );
  }
}
