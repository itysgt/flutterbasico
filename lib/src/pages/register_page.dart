import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:itys_app/src/utils/utils.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    late TextEditingController nombresInputController =
        new TextEditingController();
    late TextEditingController apellidosInputController =
        new TextEditingController();
    late TextEditingController emailInputController =
        new TextEditingController();
    late TextEditingController pwdInputController = new TextEditingController();
    late TextEditingController confirmacionPwdInputController =
        new TextEditingController();

    return Scaffold(
        appBar: AppBar(
          title: Text("Registrar"),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: [
                  SizedBox(height: 15.0),
                  Text(
                    "Registro de nuevo usuario",
                    textAlign: TextAlign.center,
                    style: inputLabelStyle,
                  ),
                  SizedBox(height: 15.0),
                  TextFormField(
                    decoration: InputDecoration(
                        isDense: true,
                        prefixIcon: Icon(Icons.person_rounded),
                        labelText: 'Nombres',
                        hintText: "Nombres"),
                    controller: nombresInputController,
                  ),
                  SizedBox(height: 15.0),
                  TextFormField(
                    decoration: InputDecoration(
                        isDense: true,
                        prefixIcon: Icon(Icons.person),
                        labelText: 'Apellidos*',
                        hintText: "Apellidos"),
                    controller: apellidosInputController,
                  ),
                  SizedBox(height: 15.0),
                  TextFormField(
                    decoration: InputDecoration(
                        isDense: true,
                        prefixIcon: Icon(Icons.email),
                        labelText: 'Correo*',
                        hintText: "usuario@gmail.com"),
                    keyboardType: TextInputType.emailAddress,
                    controller: emailInputController,
                  ),
                  SizedBox(height: 15.0),
                  TextFormField(
                    decoration: InputDecoration(
                        isDense: true,
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: 'Contraseña*',
                        hintText: "********"),
                    obscureText: true,
                    controller: pwdInputController,
                  ),
                  SizedBox(height: 15.0),
                  TextFormField(
                    decoration: InputDecoration(
                        isDense: true,
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: 'Confirmar Contraseña*',
                        hintText: "********"),
                    obscureText: true,
                    controller: confirmacionPwdInputController,
                  ),
                  SizedBox(height: 40.0),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: StadiumBorder(), primary: Colors.blue),
                      child: Text("Guardar"),
                      onPressed: () {
                        if (nombresInputController.text.isNotEmpty &&
                            apellidosInputController.text.isNotEmpty &&
                            emailInputController.text.isNotEmpty &&
                            pwdInputController.text.isNotEmpty &&
                            confirmacionPwdInputController.text.isNotEmpty) {
                          if (pwdInputController.text ==
                              confirmacionPwdInputController.text) {
                            FirebaseAuth.instance
                                .createUserWithEmailAndPassword(
                                    email: emailInputController.text,
                                    password: pwdInputController.text)
                                .then((currentUser) => FirebaseFirestore
                                    .instance
                                    .collection("users")
                                    .doc(currentUser.user!.uid)
                                    .set({
                                      "uid": currentUser.user!.uid,
                                      "nombre": nombresInputController.text,
                                      "apellido": apellidosInputController.text,
                                      "email": emailInputController.text
                                    })
                                    .then((result) => {Navigator.pop(context)})
                                    .catchError((err) => print(err)))
                                .catchError((err) => print(err));
                          } else {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Error"),
                                    content:
                                        Text("Las contraseñas no coinciden"),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text("Cerrar"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      )
                                    ],
                                  );
                                });
                          }
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(
                                  "Ingrese los datos necesarios para el registro")));
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }
}
