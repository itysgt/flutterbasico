import 'package:flutter/material.dart';
import 'package:itys_app/src/pages/home_page.dart';
import 'package:itys_app/src/pages/register_page.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:itys_app/src/widgets/buttons.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  late TextEditingController emailInputController = new TextEditingController();
  late TextEditingController pwdInputController = new TextEditingController();

  // @override
  // void initState() {
  //   emailInputController = new TextEditingController();
  //   pwdInputController = new TextEditingController();

  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Login"),
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                      child:
                          Image.asset("assets/images/itys.png", width: 350.0)),
                  SizedBox(height: 40.0),
                  TextFormField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        labelText: "Correo",
                        hintText: "usuario@localhost.com"),
                    keyboardType: TextInputType.emailAddress,
                    controller: emailInputController,
                  ),
                  SizedBox(height: 20.0),
                  TextFormField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.vpn_key),
                        labelText: "Contraseña",
                        hintText: "**********"),
                    keyboardType: TextInputType.emailAddress,
                    obscureText: true,
                    controller: pwdInputController,
                  ),
                  SizedBox(height: 40.0),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: StadiumBorder(), primary: Colors.blue),
                      child: Text("Iniciar Sesión"),
                      onPressed: () {
                        if (emailInputController.text.isNotEmpty &&
                            pwdInputController.text.isNotEmpty) {
                          FirebaseAuth.instance
                              .signInWithEmailAndPassword(
                                  email: emailInputController.text,
                                  password: pwdInputController.text)
                              .then((currentUser) => Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => HomePage())));
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Complete sus datos"),
                            backgroundColor: Colors.red,
                          ));
                        }
                      },
                    ),
                  ),
                  SizedBox(
                      width: double.infinity,
                      child: GoogleButton('Entrar con Facebook', () {})),
                  SizedBox(
                      width: double.infinity,
                      child: FacebookButton('Entrar con Facebook', () {})),
                  SizedBox(height: 40.0),
                  Text("¿Todavía no tienes una cuenta?"),
                  TextButton(
                      child: Text("!Regístrate aquí!"),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegisterPage()));
                      }),
                  SizedBox(height: 20.0),
                ],
              ),
            ),
          ],
        ));
  }
}
