import 'package:flutter/material.dart';

const celesteInputs = Color(0xFFECF1F7);
const inputLabelColor = Color(0xFF919090);

var inputLabelStyle = TextStyle(
    fontFamily: 'spartan',
    fontSize: 20,
    fontWeight: FontWeight.w600,
    color: Colors.black54);
