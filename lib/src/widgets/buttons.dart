import 'package:flutter/material.dart';

class FacebookButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback onPressed;
  const FacebookButton(this.buttonText, this.onPressed, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      splashColor: Colors.white,
      color: Color(0xFF1877f2),
      onPressed: onPressed,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40)),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(image: AssetImage("assets/images/fb_logo.png"), height: 20.0),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              '$buttonText',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Roboto-Medium',
                  fontSize: 14),
            ),
          )
        ],
      ),
    );
  }
}

class GoogleButton extends StatelessWidget {
  final String buttonText;
  final VoidCallback onPressed;
  const GoogleButton(this.buttonText, this.onPressed, {Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      splashColor: Colors.grey,
      onPressed: onPressed,
      color: Colors.white,
      shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.white, width: 1.0),
          borderRadius: BorderRadius.circular(40)),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image(
              image: AssetImage("assets/images/google_logo.png"), height: 20.0),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(
              '$buttonText',
              style: TextStyle(
                  color: Colors.black.withOpacity(0.54),
                  fontFamily: 'Roboto-Medium',
                  fontSize: 18),
            ),
          )
        ],
      ),
    );
  }
}
